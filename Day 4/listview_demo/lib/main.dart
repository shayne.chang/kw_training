import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter ListView Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required this.title}) : super();

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(child: firstListView()),
    );
  }
}

Widget firstListView() {
  return ListView(
    padding: const EdgeInsets.all(8),
    children: <Widget>[
      for (var i = 0; i < 20; i++)
        Container(
          height: 50,
          // color: i % 2 == 0 ? Colors.amber[100] : Colors.pink[100],
          child: Center(child: Text('$i')),
        ),
    ],
  );
}

Widget listViewExample() {
  return ListView(
    itemExtent: 100,
    scrollDirection: Axis.horizontal,
    padding: const EdgeInsets.all(8),
    children: <Widget>[
      for (var i = 0; i < 20; i++)
        Container(
          height: 100,
          // width: 50,
          color: i % 2 == 0 ? Colors.grey[100] : Colors.blueGrey[100],
          child: Center(child: Text('$i')),
        ),
    ],
  );
}

Widget builderListView() {
  return ListView.builder(
    itemCount: 20,
    padding: const EdgeInsets.all(8),
    itemBuilder: (context, index) {
      print('create $index');
      return Container(
          height: 50,
          color: index % 2 == 0 ? Colors.amber[100] : Colors.pink[100],
          child: Text('child$index'));
    },
  );
}
