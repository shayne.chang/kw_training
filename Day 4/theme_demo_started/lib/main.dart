import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyHomePage(title: 'Flutter Theme Demo');
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required this.title}) : super();
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isDarkMode = false; //記錄現在的模式是否為夜間模式
  String currentTheme = 'Light'; //顯示目前的模式
  IconData darkModeIcon = Icons.nightlight_round; //下方切換模式的IconButton

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            brightness: Brightness.light,
            primarySwatch: Colors.amber,
            textTheme: TextTheme(
                headlineLarge:
                    TextStyle(fontSize: 100, fontWeight: FontWeight.w200),
                headlineMedium: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue))),
        darkTheme: ThemeData(brightness: Brightness.dark),
        home: Builder(builder: (context) {
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.title),
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    'Current mode is',
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  Text(
                    '$currentTheme',
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  IconButton(
                      icon: Icon(darkModeIcon, color: Colors.grey),
                      onPressed: () {
                        setState(() {
                          if (!isDarkMode) {
                            currentTheme = 'Dark';
                            darkModeIcon = Icons.wb_sunny;
                          } else {
                            currentTheme = 'Light';
                            darkModeIcon = Icons.nightlight_round;
                          }
                          isDarkMode = !isDarkMode;
                        });
                      }),
                ],
              ),
            ),
          );
        }));
  }
}
