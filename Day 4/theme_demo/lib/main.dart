import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyHomePage(title: 'Flutter Theme Demo');
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required this.title}) : super();
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final lightModeTheme = ThemeData(
      primarySwatch: Colors.amber,
      brightness: Brightness.light,
      textTheme: TextTheme(
          headline1: TextStyle(color: Colors.amber),
          headline4: TextStyle(color: Colors.blue)));
  final darkModeTheme = ThemeData(
      primarySwatch: Colors.amber,
      brightness: Brightness.dark,
      textTheme: TextTheme(headline4: TextStyle(color: Colors.lime)));
  bool isDarkMode = false;
  String currentTheme = 'Light';
  IconData darkModeIcon = Icons.nightlight_round;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: isDarkMode ? darkModeTheme : lightModeTheme,
      home: Builder(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text('Current mode is',
                    style: Theme.of(context).textTheme.headline4),
                Text('$currentTheme',
                    style: Theme.of(context).textTheme.headline1),
                IconButton(
                    icon: Icon(darkModeIcon, color: Colors.grey),
                    onPressed: () {
                      print(isDarkMode);
                      setState(() {
                        if (!isDarkMode) {
                          currentTheme = 'Dark';
                          darkModeIcon = Icons.wb_sunny;
                        } else {
                          currentTheme = 'Light';
                          darkModeIcon = Icons.nightlight_round;
                        }
                        isDarkMode = !isDarkMode;
                      });
                    }),
              ],
            ),
          ),
        );
      }),
    );
  }
}
