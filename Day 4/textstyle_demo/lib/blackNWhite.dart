import 'package:flutter/material.dart';

Widget blackNWhiteDemo() {
  return Stack(
    children: [
      Container(
          color: Colors.amber,
          width: 100,
          height: 100,
          child: Text('Some Text')),
      Container(
        height: 200,
        width: 200,
        color: Colors.black45,
      ),
    ],
  );
}
