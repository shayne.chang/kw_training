import 'package:flutter/material.dart';

Widget colorsDemo() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
          height: 100,
          width: 200,
          color: Colors.blue,
          child: Text('Colors.blue')),
      Container(
          height: 100,
          width: 200,
          color: Colors.blue[50],
          child: Text('Colors.blue[50]')),
      Container(
          height: 100,
          width: 200,
          color: Colors.blue[100],
          child: Text('Colors.blue[100]')),
      Container(
          height: 100,
          width: 200,
          color: Colors.blue[400],
          child: Text('Colors.blue[400]')),
      Container(
          height: 100,
          width: 200,
          color: Colors.blue[900],
          child: Text('Colors.blue[900]')),
    ],
  );
}


      // Container(
      //     height: 100,
      //     width: 200,
      //     color: Colors.blue,
      //     child: Text('Colors.blue')),
      // Container(
      //     height: 100,
      //     width: 200,
      //     color: Colors.blueAccent,
      //     child: Text('Colors.blue[50]')),
      // Container(
      //     height: 100,
      //     width: 200,
      //     color: Colors.blue[100],
      //     child: Text('Colors.blue[100]')),
      // Container(
      //     height: 100,
      //     width: 200,
      //     color: Colors.blue[400],
      //     child: Text('Colors.blue[400]')),
      // Container(
      //     height: 100,
      //     width: 200,
      //     color: Colors.blue[900],
      //     child: Text('Colors.blue[900]')),