// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

Widget textStyleDemo() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        'Hello World!',
      ),
      // Text(
      //   '原始文字',
      //   style: TextStyle(fontWeight: FontWeight.bold),
      // ),
      // Text(
      //   'fontWeight: FontWeight.w100',
      //   style: TextStyle(fontWeight: FontWeight.w100),
      // ),
      // Text('fontWeight: FontWeight.w200',
      //     style: TextStyle(fontWeight: FontWeight.w200)),
      // Text(
      //   'fontWeight: FontWeight.w300',
      //   style: TextStyle(fontWeight: FontWeight.w300),
      // ),
      // Text('fontWeight: FontWeight.w400',
      //     style: TextStyle(fontWeight: FontWeight.w400)),
      // Text('fontWeight: FontWeight.w500',
      //     style: TextStyle(fontWeight: FontWeight.w500)),
      // Text('fontWeight: FontWeight.w600',
      //     style: TextStyle(fontWeight: FontWeight.w600)),
      // Text('fontWeight: FontWeight.w700',
      //     style: TextStyle(fontWeight: FontWeight.w700)),
      // Text('fontWeight: FontWeight.w800',
      //     style: TextStyle(fontWeight: FontWeight.w800)),
      // Text('fontWeight: FontWeight.w900',
      //     style: TextStyle(fontWeight: FontWeight.w900)),
      // Text(
      //   'fontStyle: FontStyle.italic',
      //   style: TextStyle(fontStyle: FontStyle.italic),
      // ),
      // Text(
      //   'fontFamily: ‘Georgia’',
      //   style: TextStyle(fontSize: 50, fontFamily: 'Georgia'),
      // ),
      // Text(
      //   'fontFamily: ‘Georgia’',
      //   style: TextStyle(
      //     fontSize: 50,
      //   ),
      // ),
      // Text(
      //   'backgroundColor: Colors.amber[100]',
      //   style: TextStyle(backgroundColor: Colors.amber[100]),
      // ),
      // Text(
      //   'letterSpacing: 10',
      //   style: TextStyle(fontSize: 30, letterSpacing: 10),
      // ),
      // Text(
      //   'Hello World',
      //   style: TextStyle(fontSize: 30, wordSpacing: 50),
      // ),
      // Text(
      //   'Hello World',
      //   style: TextStyle(
      //       fontSize: 50,
      //       shadows: [Shadow(color: Colors.amber, offset: Offset(5, 5))]),
      // ),
    ],
  );
}
