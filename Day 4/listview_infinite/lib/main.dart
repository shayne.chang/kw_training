import 'dart:math';

import 'package:flutter/material.dart';

import 'model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Infinite ListView Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required this.title}) : super();

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _controller = ScrollController();
  List<RandomData> allData = List.generate(
      15,
      (index) => RandomData('Amy$index', 20 + Random().nextInt(40),
          35 + Random().nextInt(100), 130 + Random().nextInt(70)));
  bool isLoading = false;
  int startIndex = 15;

  fetchData(int start) {
    isLoading = true;
    List<RandomData> newData = List.generate(
        15,
        (index) => RandomData('Amy${index + start}', 20 + Random().nextInt(40),
            35 + Random().nextInt(100), 130 + Random().nextInt(70)));
    startIndex += 15;
    setState(() {
      allData = [...allData, ...newData];
      isLoading = false;
    });
  }

  @override
  void initState() {
    _controller.addListener(() {
      if (_controller.position.maxScrollExtent == _controller.offset) {
        fetchData(startIndex);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView.builder(
            controller: _controller,
            itemCount: allData.length + 1,
            itemBuilder: (context, index) {
              if (index < allData.length) {
                return ListTile(
                  title: Text('title'),
                  subtitle: Text('subtitle'),
                  leading: Text('$index'),
                  trailing: Text('trailing'),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }
}
