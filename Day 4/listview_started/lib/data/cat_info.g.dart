// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CatInfo _$CatInfoFromJson(Map<String, dynamic> json) {
  return CatInfo(
    json['animal_id'] as int,
    json['animal_Variety'] as String,
    json['animal_sex'] as String,
    json['animal_bodytype'] as String,
    json['animal_colour'] as String,
    json['animal_age'] as String,
    json['animal_createtime'] as String,
    json['shelter_name'] as String,
    json['album_file'] as String,
    json['shelter_tel'] as String,
  );
}

Map<String, dynamic> _$CatInfoToJson(CatInfo instance) => <String, dynamic>{
      'animal_id': instance.animal_id,
      'animal_Variety': instance.animal_Variety,
      'animal_sex': instance.animal_sex,
      'animal_bodytype': instance.animal_bodytype,
      'animal_colour': instance.animal_colour,
      'animal_age': instance.animal_age,
      'animal_createtime': instance.animal_createtime,
      'shelter_name': instance.shelter_name,
      'album_file': instance.album_file,
      'shelter_tel': instance.shelter_tel,
    };
