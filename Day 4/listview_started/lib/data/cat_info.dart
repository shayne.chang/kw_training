import 'package:json_annotation/json_annotation.dart';

part 'cat_info.g.dart';

List<CatInfo> getCatInfoList(List<dynamic> list) {
  List<CatInfo> result = [];
  list.forEach((item) {
    result.add(CatInfo.fromJson(item));
  });
  return result;
}

@JsonSerializable()
class CatInfo extends Object {
  int animal_id;

  String animal_Variety;

  String animal_sex;

  String animal_bodytype;

  String animal_colour;

  String animal_age;

  String animal_createtime;

  String shelter_name;

  String album_file;

  String shelter_tel;

  CatInfo(
    this.animal_id,
    this.animal_Variety,
    this.animal_sex,
    this.animal_bodytype,
    this.animal_colour,
    this.animal_age,
    this.animal_createtime,
    this.shelter_name,
    this.album_file,
    this.shelter_tel,
  );

  factory CatInfo.fromJson(Map<String, dynamic> srcJson) =>
      _$CatInfoFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CatInfoToJson(this);
}
