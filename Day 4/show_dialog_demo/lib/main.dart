import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: DialogExample(),
    );
  }
}

class DialogExample extends StatelessWidget {
  const DialogExample({key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('showDialog Sample')),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          OutlinedButton(
            onPressed: () => _buildAlertDialog(context),
            child: const Text('Open AlertDialog'),
          ),
          OutlinedButton(
            onPressed: () => _buildSimpleDialog(context),
            child: const Text('Open SimpleDialog'),
          ),
        ]),
      ),
    );
  }

  Future<void> _buildAlertDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierColor: Colors.white60,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          icon: Icon(Icons.warning),
          title: const Text('可以放圖片'),
          content: Image.asset('lib/assets/hello.png'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }

  Future<void> _buildSimpleDialog(context) {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(title: const Text('SimpleDialog'), children: [
            for (var i = 0; i < 50; i++)
              SimpleDialogOption(
                child: Text('Country $i'),
                onPressed: () {
                  print(i);
                  Navigator.of(context).pop();
                },
              )
          ]);
        });
  }
}
