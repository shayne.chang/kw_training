// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(body: Demo()),
    );
  }
}

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  double _top = 100;
  double _left = 100;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
            top: _top,
            left: _left,
            child: GestureDetector(
              onPanUpdate: (details) {
                // print(details.delta.dx);
                setState(() {
                  _left = _left + details.delta.dx;
                  _top += details.delta.dy;
                });
              },
              onPanEnd: (details) {
                setState(() {
                  _top = 100;
                  _left = 100;
                });
              },
              child: CircleAvatar(
                child: Icon(Icons.message),
              ),
            )),
      ],
    );
  }
}
