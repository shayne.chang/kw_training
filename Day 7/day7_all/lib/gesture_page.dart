import 'package:flutter/material.dart';

class GestureDemo extends StatefulWidget {
  const GestureDemo({super.key});

  @override
  State<GestureDemo> createState() => _GestureDemoState();
}

class _GestureDemoState extends State<GestureDemo> {
  String _showGesture = 'No Gesture detected';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        // onScaleStart: (details) => setState(() {
        //   print(details.localFocalPoint); //焦點
        // }),
        onScaleUpdate: (details) => setState(() {
          print(details.rotation);
        }),
        // onScaleEnd: (details) => setState(() {}),
        child: Container(
            alignment: Alignment.center,
            color: Colors.green[100],
            height: 200,
            width: 300,
            child: Text(_showGesture, style: const TextStyle(fontSize: 25))),
      ),
    );
  }
}

// ==== onTap ====
//  onTap: () => setState(() {
//         _showGesture = 'Tap';
//       }),
//       onTapDown: (details) {
//         setState(() {
//           _showGesture = 'onTapDown';
//         });
//       },
//       onTapUp: (details) => setState(() {
//         _showGesture = 'onTapUp';
//       }),
//       onTapCancel: () => setState(() {
//         _showGesture = 'onTapCancel';
//       }),

// === 
      // onDoubleTap: () => setState(() {
      //   _showGesture = 'onDoubleTap';
      // }),


// == longpress
    //  onLongPress: () => setState(() {
    //     _showGesture = 'onLongPress';
    //   }),
    //   onLongPressDown: (e) => setState(() {
    //     _showGesture = 'onLongPressDown';
    //   }),
    //   onLongPressUp: () => setState(() {
    //     _showGesture = 'onLongPressUp';
    //   }),
    //   onLongPressCancel: () => setState(() {
    //     _showGesture = 'onLongPressCancel';
    //   }),


    //onVerticalDragDown

      //    onVerticalDragDown: (details) => setState(() {
      //   _showGesture = 'onVerticalDragDown';
      // }),
      // onVerticalDragStart: (details) => setState(() {
      //   _showGesture = 'onVerticalDragStart';
      // }),
      // onVerticalDragUpdate: (details) => setState(() {
      //   _showGesture = 'onVerticalDragUpdate ${details.delta}';
      // }),
      // onVerticalDragEnd: (details) => setState(() {
      //   _showGesture = 'onVerticalDragEnd';
      // }),

  // onPan 

      //   onPanDown: (details) => setState(() {
      //   _showGesture = 'onPanDown';
      //   print('onPanDown');
      // }),
      // onPanStart: (details) => setState(() {
      //   _showGesture = 'onPanStart';
      //   print('onPanStart');
      // }),
      // onPanUpdate: (details) => setState(() {
      //   _showGesture = 'onPanUpdate';
      //   print('onPanUpdate');
      // }),
      // onPanEnd: (details) => setState(() {
      //   _showGesture = 'onPanEnd';
      //   print('onPanEnd');
      // }),
      // onTapDown: (details) {
      //   setState(() {
      //     _showGesture = 'onTapDown';
      //     print('onTapDown');
      //   });
      // },
      // onTap: () {
      //   setState(() {
      //     _showGesture = 'onTap';
      //     print('onTap');
      //   });
      // },

  // scale
      //   onScaleStart: (details) => setState(() {
      //   print(details.focalPoint); //焦點
      // }),
      // onScaleUpdate: (details) => setState(() {
      //   print(details.rotation);
      // }),
      // onScaleEnd: (details) => setState(() {}),