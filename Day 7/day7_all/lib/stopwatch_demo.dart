import 'dart:async';

import 'package:flutter/material.dart';

class StopWatchDemo extends StatefulWidget {
  const StopWatchDemo({super.key});

  @override
  State<StopWatchDemo> createState() => _StopWatchDemoState();
}

class _StopWatchDemoState extends State<StopWatchDemo> {
  String _timePassed = '0';
  final stopwatch = Stopwatch();
  late Timer _timer;
  void startT() {
    stopwatch.start();
    _timer = Timer.periodic(Duration(milliseconds: 100), (timer) {
      setState(() {
        _timePassed = stopwatch.elapsed.toString();
      });
    });
  }

  void stopT() {
    stopwatch.stop();
    stopwatch.reset();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextButton(
            onPressed: startT,
            child: const Text(
              'Start',
              style: TextStyle(fontSize: 30),
            )),
        Text(_timePassed, style: TextStyle(fontSize: 30)),
        TextButton(
            onPressed: stopT,
            child: const Text(
              'Stop',
              style: TextStyle(fontSize: 30),
            )),
      ],
    ));
  }
}
