import 'dart:async';

import 'package:flutter/material.dart';

class TimerDemo extends StatefulWidget {
  const TimerDemo({super.key});

  @override
  State<TimerDemo> createState() => _TimerDemoState();
}

class _TimerDemoState extends State<TimerDemo> {
  int _time = 0;
  String _usingTenSec = '否';
  late Timer myTimer;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _time += 1;
        if (_time >= 10) {
          _usingTenSec = '是';
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '$_time sec passed.',
          style: const TextStyle(fontSize: 30),
        ),
        Text(
          '已在此頁面停留10秒 ? $_usingTenSec',
          style: const TextStyle(fontSize: 30),
        ),
        TextButton(
            onPressed: () {
              if (myTimer.isActive) {
                myTimer.cancel();
              }
            },
            child: Text('停止計時'))
      ],
    ));
  }
}
