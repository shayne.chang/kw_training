import 'package:flutter/material.dart';

class InteractiveViewDemo extends StatelessWidget {
  const InteractiveViewDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      width: 300,
      height: 300,
      color: Colors.amber,
      child: InteractiveViewer(
          boundaryMargin: EdgeInsets.all(double.infinity),
          maxScale: 20.0,
          minScale: 0.5,
          // panEnabled: false,
          scaleEnabled: false,
          child: Image.network('https://pse.is/4ryec3')),
    )
        // ),
        );
  }
}
