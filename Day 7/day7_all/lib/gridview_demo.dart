import 'package:flutter/material.dart';

class GridViewDemo extends StatefulWidget {
  const GridViewDemo({super.key});

  @override
  State<GridViewDemo> createState() => _GridViewDemoState();
}

class _GridViewDemoState extends State<GridViewDemo> {
  List<String> list = ['A', 'B', 'C', 'D'];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: list.length,
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
        itemBuilder: ((context, index) {
          return GestureDetector(
            onDoubleTap: () {
              setState(() {
                list.removeAt(index);
              });
            },
            child: Container(
                color: Colors.lightGreen[index * 100],
                child: Center(child: Text(list[index]))),
          );
        }));
  }
}
