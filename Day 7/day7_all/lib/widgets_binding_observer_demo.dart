import 'package:flutter/material.dart';

class WBDemo extends StatefulWidget {
  const WBDemo({super.key});

  @override
  State<WBDemo> createState() => _WBDemoState();
}

class _WBDemoState extends State<WBDemo> with WidgetsBindingObserver {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    if (state == AppLifecycleState.resumed) {
      print('重新打開app，發API');
    }
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}




















// class WidgetsBindingObserverDemo extends StatefulWidget {
//   const WidgetsBindingObserverDemo({super.key});

//   @override
//   State<WidgetsBindingObserverDemo> createState() =>
//       _WidgetsBindingObserverDemoState();
// }

// class _WidgetsBindingObserverDemoState extends State<WidgetsBindingObserverDemo>
//     with WidgetsBindingObserver {
//   //1. 在class上加入 with WidgetsBindingObserver

//   @override
//   void initState() {
//     super.initState();
//     WidgetsBinding.instance.addObserver(this);
//     //2. 在initState時，加入addObserver
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     WidgetsBinding.instance.removeObserver(this);
//     //3. 在dispose時，removeObserver
//   }

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) {
//     print("$state");
//     //4. 在這個function中實作想要的操作
//   }

//   @override
//   Widget build(BuildContext context) {
//     return const MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           body: Center(child: Text('WidgetsBindingObserverDemo')),
//         ));
//   }
// }
