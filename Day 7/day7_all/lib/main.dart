// ignore_for_file: prefer_const_constructors
import 'package:day7_all/edge_insets_demo.dart';
import 'package:day7_all/interactive_view_demo.dart';
import 'package:day7_all/permission_demo.dart';
import 'package:day7_all/stopwatch_demo.dart';
import 'package:day7_all/widgets_binding_observer_demo.dart';
import 'package:flutter/material.dart';

import 'gesture_recap1.dart';
import 'gesture_page.dart';
import 'gesture_recap2.dart';
import 'gridview_demo.dart';
import 'media_query_demo.dart';
import 'pointer_page.dart';
import 'timer_demo.dart';

void main() {
  runApp(const MainApp()

      // WBDemo()
      );
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          body:
              //  GridViewDemo()
              // StopWatchDemo()
              TimerDemo()
          // PermissionDemo()
          // MediaQueryDemo()
          // EdgeInsetsDemo()
          // InteractiveViewDemo()
          // GestureRecapScale() //放大縮小
          // GestureRecapDrag() //可拖動的懸浮物件
          // GestureDemo()
          // pointerPage()
          //
          ),
    );
  }
}
