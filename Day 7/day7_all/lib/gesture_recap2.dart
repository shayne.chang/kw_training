import 'package:flutter/material.dart';

class GestureRecapScale extends StatefulWidget {
  const GestureRecapScale({Key? key}) : super(key: key);

  @override
  _GestureRecapScaleState createState() => _GestureRecapScaleState();
}

class _GestureRecapScaleState extends State<GestureRecapScale> {
  double _width = 200.0; //設定圖片寬度

  @override
  Widget build(BuildContext context) {
    return Center(
        child: GestureDetector(
      onScaleUpdate: (details) {
        setState(() {
          if (details.scale > 1.0) {
            _width = 200 * details.scale;
          }
        });
      },
      onScaleEnd: (details) {
        setState(() {
          _width = 200;
        });
      },
      child: Image.network(
        'https://pse.is/4ryec3',
        width: _width,
      ),
    ));
  }
}
