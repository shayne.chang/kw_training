// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionDemo extends StatefulWidget {
  const PermissionDemo({super.key});

  @override
  State<PermissionDemo> createState() => _PermissionDemoState();
}

class _PermissionDemoState extends State<PermissionDemo> {
  void getPermission() async {
    PermissionStatus _status = await Permission.location.status;
    print(_status);
    _status = await Permission.location.request();
    print(_status);
    if (_status.isGranted) {
      print('拿到權限');
    }
    if (_status.isDenied) {
      print('使用者拒絕');
    }
    if (_status.isPermanentlyDenied) {
      settingDialog();
      print('不要再問');
    }
  }

  String _permission = 'check permission';
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(_permission, style: const TextStyle(fontSize: 20)),
        TextButton(
            onPressed: getPermission,
            child: const Text(
              '取得位置權限',
              style: TextStyle(fontSize: 20),
            )),
      ],
    ));
  }

  void settingDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('開啟系統設定'),
            content: const Text('需開啟系統設定來給予權限'),
            actions: <Widget>[
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: Theme.of(context).textTheme.labelLarge,
                ),
                child: const Text('取消'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: Theme.of(context).textTheme.labelLarge,
                ),
                child: const Text('確定'),
                onPressed: () {
                  openAppSettings();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}


    // PermissionStatus status = await Permission.location.status;
    // print(status);
    // status = await Permission.location.request();
    // print(status);

    // if (status.isDenied) {
    //   print('拒絕授權');
    // }
    // if (status.isPermanentlyDenied) {
    //   print('永遠拒絕');
    // }
    // setState(() {
    //   _permission = status.toString();
    // });