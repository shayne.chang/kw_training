import 'package:flutter/material.dart';

class EdgeInsetsDemo extends StatelessWidget {
  const EdgeInsetsDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.amber,
        // padding: EdgeInsets.all(20),
        child: Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            color: Colors.blueGrey,
            width: 300,
            height: 200,
            child: Image.network('https://pse.is/4ryec3')),
      ),
    );
  }
}
