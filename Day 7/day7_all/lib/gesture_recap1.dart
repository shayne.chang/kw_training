import 'package:flutter/material.dart';

class GestureRecapDrag extends StatefulWidget {
  const GestureRecapDrag({super.key});

  @override
  State<GestureRecapDrag> createState() => _GestureRecapDragState();
}

class _GestureRecapDragState extends State<GestureRecapDrag> {
  double _top = 100.0;
  double _left = 20.0;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
            top: _top,
            left: _left,
            child: GestureDetector(
                onPanDown: (details) => print(details),
                onPanUpdate: (details) {
                  print(details);
                  setState(() {
                    _top += details.delta.dy;
                    _left += details.delta.dx;
                  });
                },
                child: CircleAvatar(child: Icon(Icons.message)))),
      ],
    );
  }
}
