import 'package:flutter/material.dart';

class MediaQueryDemo extends StatelessWidget {
  const MediaQueryDemo({super.key});

  @override
  Widget build(BuildContext context) {
    final _widthOfScreen = MediaQuery.of(context).size.width;
    final _isPhone = MediaQuery.of(context).size.width < 500;
    return Center(
        child: Container(
            color: Colors.pink[50],
            height: 300,
            child: _isPhone
                ? Text(
                    '現在使用的是手機',
                    style: TextStyle(fontSize: 30),
                  )
                : Text('使用的是電腦')));
  }
}
